// Sanbercode
// Kelas React Native - Mobile App Development
// Tugas 9 – Class
// Oleh: Muhammad Nevin

// 1. Animal Class 
console.log('\n1. Animal Class \n');
class Animal {
    constructor(_name) {
        this._name = _name
        this._legs = 4
        this._cold_blooded = false
    }
    get name() {
        return this._name
    }
    set name(_name) {
        this._name = _name
    }
    get legs() {
        return this._legs
    }
    set legs(_legs) {
        this._legs = _legs
    }
    get cold_blooded() {
        return this._cold_blooded
    }
    set cold_blooded(cold_blooded) {
        this._cold_blooded = this._cold_blooded
    }
}

console.log('-> Release 0\n');
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log('-> Release 1\n');
// Code class Ape dan class Frog di sini
class Ape extends Animal{
    constructor(_name) {
        super(_name)
        this.legs = 2
    }
    yell() {
        console.log('Auooo');
    }
}
class Frog extends Animal {
    constructor(_name) {
        super(_name)
    }
    jump() {
        console.log('hop hop');
    }
}
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
// console.log(sungokong._legs);
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

// 2. Function to Class
console.log('\n2. Function to Class\n');

class Clock {
    constructor({template}) {
        this._template = template
    }
    render() {
        var date = new Date();
    
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
    
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
    
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
        
        var output = this._template
          .replace('h', hours)
          .replace('m', mins)
          .replace('s', secs);
    
        console.log("render ->", output);
    }
    stop() {
        clearInterval(_timer);
    }
    start() {
        this.render;
        this._timer = setInterval(() => this.render(), 1000);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 