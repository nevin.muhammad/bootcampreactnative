// Sanbercode
// Kelas React Native - Mobile App Development
// Tugas 3 – Looping
// Oleh: Muhammad Nevin


// No. 1 Looping While 
console.log("No. 1 Looping While\n");
var i = 2;

console.log("LOOPING PERTAMA");
while(i <= 20) {
    console.log(i + " - I love coding");
    i += 2;
}
console.log("LOOPING KEDUA");
while(i >= 2) {
    i -= 2;
    console.log(i + " - I will become a mobile developer");
}

// No. 2 Looping menggunakan for
console.log("\nNo. 2 Looping menggunakan for\n");
for (var j = 1; j <= 20; j++) {
    process.stdout.write(j + " - ")
    if(j % 3 == 0 && j % 2 == 1) { 
        console.log("I Love Coding");
    } else if (j % 2) {
        console.log("Santai");
    } else {
        console.log("Berkualitas");
    }
}

// No. 3 Membuat Persegi Panjang
console.log("\nNo. 3 Membuat Persegi Panjang\n");
for(var a = 0; a < 4; a++) {
    for(var b = 0; b < 8; b++) {
        process.stdout.write("#");
    }
    console.log("");
}

// No. 4 Membuat Tangga
console.log("\nNo. 4 Membuat Tangga\n");
for(var x = 0; x < 7; x++) {
    for(var y = 0; y <= x; y++) {
        process.stdout.write("*");
    }
    console.log("");
}

// No. 5 Membuat Papan Catu
console.log("\nNo. 5 Membuat Papan Catu\n");
for(var m = 0; m < 8; m++) {
    for(var n = 0; n < 8; n++) {
        if(m % 2) {
            process.stdout.write("# ");
        }
        else {
            process.stdout.write(" #");
        }
    }
    console.log("");
}