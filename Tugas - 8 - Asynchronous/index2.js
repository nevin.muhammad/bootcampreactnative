// Sanbercode
// Kelas React Native - Mobile App Development
// Tugas 8 – Asynchronous
// index2.js
// Oleh: Muhammad Nevin
// file

let readBooksPromise = require('./promise.js')
 
let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

function readBooks() {
    readBooksPromise(10000, books[0])
        .then(function(fullfilled){
            readBooksPromise(fullfilled, books[1])
                .then(function(fullfilled) {
                    readBooksPromise(fullfilled, books[2])
                })
                .catch(function(error) {})
        })
        .catch(function(error) {
            readBooksPromise(fullfilled, books[1])
                .then(function(fullfilled) {
                    readBooksPromise(fullfilled, books[2] )
                })
                .catch(function(error) {})
        })
}

readBooks()