// Sanbercode
// Kelas React Native - Mobile App Development
// Tugas 6 – Object
// Oleh: Muhammad Nevin

// Soal No. 1 (Array to Object)
console.log("Soal No. 1 (Array to Object)\n");

function arrayToObject(arr) {
    // Code di sini 
    for(var i = 0; i < arr.length; i++) {
        obj = {}
        if(arr[i][0] === 'undefined') {
            console.log("")
            return
        }
        var name = (arr[i][0] + " " + arr[i][1])
        var firstName = arr[i][0]
        var lastName = arr[i][1]
        var gender = arr[i][2]
        var age = arr[i][3]
        if(age = 'undefined') age = "Invalid Birth Year"
        obj["firstName"] = firstName
        obj["lastName"] = lastName
        obj["gender"] = gender
        obj["age"] = age
        console.log((i+1) + ". " + name + ":", obj);
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

// Soal No. 2 (Shopping Time)
console.log("\nSoal No. 2 (Shopping Time)\n");

function shoppingTime(memberId = '', money = 0) {
    if(memberId == '') return "Mohon maaf, toko X hanya berlaku untuk member saja"
    var harga = {"Sepatu Stacattu" : 1500000,
            "Baju Zoro" : 500000,
            "Baju HnN" : 250000,
            "Sweater Uniklooh" : 175000,
            "Casing Handphone" : 50000}
    var barang = ["Sepatu Stacattu", "Baju Zoro", "Baju HnN", "Sweater Uniklooh", "Casing Handphone"]
    var purchase = {}
    lp = []
    var temp = money
    for(var i = 0; i < barang.length; i++) {
        if(harga[barang[i]] <= temp) {
            lp.push(barang[i])
            temp -= harga[barang[i]]
        }
    }
    purchase["memberId"] = memberId
    purchase["money"] = money
    purchase["listPurchased"] = lp
    purchase["changeMoney"] = temp
    
    if(lp.length == 0) return "Mohon maaf, uang tidak cukup"
    return purchase
}
 
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// Soal No. 3 (Naik Angkot)
console.log("\nSoal No. 3 (Naik Angkot)\n");
function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var data = []
    for(var i = 0; i < arrPenumpang.length; i++) {
        p = {}
        p["penumpang"] = arrPenumpang[i][0]
        p["naikDari"] = arrPenumpang[i][1]
        p["tujuan"] = arrPenumpang[i][2]
        bayar = 0;
        for(var j = rute.indexOf(arrPenumpang[i][1]); j < rute.length; j++) {
            if(arrPenumpang[i][2] == rute[j]) {
                break;
            }
            else {
                bayar += 2000
            }
        }
        p["bayar"] = bayar 
        data.push(p)
    }
    return data
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]