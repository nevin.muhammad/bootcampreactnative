// Sanbercode
// Kelas React Native - Mobile App Development
// Tugas 7 – ES6
// Oleh: Muhammad Nevin

// 1. Mengubah fungsi menjadi fungsi arrow
console.log("\n1. Mengubah fungsi menjadi fungsi arrow\n");
const golden = () => console.log("this is golden!!")
   
golden()

// 2. Sederhanakan menjadi Object literal di ES6
console.log("\n2. Sederhanakan menjadi Object literal di ES6\n");

const newFunction = function literal(firstName, lastName){
    return {
        firstName,
        lastName,
        fullName : () => console.log(`${firstName} ${lastName}`) 
    }
}
   
//Driver Code 
newFunction("William", "Imoh").fullName() 

// 3. Destructuring
console.log("\n3. Destructuring\n");
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
  
// Driver code
const {firstName, lastName, destination, occupation} = newObject
console.log(firstName, lastName, destination, occupation)

// 4. Array Spreading
console.log("\n4. Array Spreading\n");
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)

// 5. Template Literals
console.log("\n5. Template Literals\n");
const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
 
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
// Driver Code
console.log(before)